# -*- coding: utf-8 -*-
import Adafruit_BBIO.GPIO as GPIO
import time
import sys
import os
import django

base_path = os.path.dirname(os.path.abspath(__file__))
sys.path.append(base_path + '/../liocontrol')
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'liocontrol.settings')
django.setup()
from panel.models import Person


UNKNOWN = 0
CLOSED  = 1
OPEN    = 2
MOVING  = 3


# Bitmap
## 7: Manual mode
## 1: Night time
## 0: Raining
STATE_OPEN          = 0b00000000
STATE_RAINING       = 0b00000001
STATE_NIGHT         = 0b00000010
STATE_MANUAL_CLOSED = 0b00000100
STATE_MANUAL_OPEN   = 0b00001000
STATE_FAIL          = 0b10000000
#TODO: começar a usar a mascara nos if's
STATE_MASK_CLOSE    = 0b10001111

MODE_AUTO           = 0b00000000
MODE_MANUAL_OPEN    = 0b10000000
MODE_MANUAL_CLOSED  = 0b10000001
MODE_NULL           = 0b11111111

def switch(expectedState, lastSwitch):
    now = int(time.time())
    if(expectedState == OPEN):
        print str(now) + ': OPEN'
        if(abs(now - lastSwitch) >= 120):
            lastSwitch = now;
            GPIO.output('P8_16', GPIO.LOW)
            GPIO.cleanup()
            time.sleep(1)
            GPIO.output('P8_16', GPIO.HIGH)
            GPIO.cleanup()
            time.sleep(1)
    else:
        print str(now) + ': CLOSE'
        if(abs(now - lastSwitch) >= 15):
            lastSwitch = now;
            GPIO.output('P8_16', GPIO.LOW)
            GPIO.cleanup()
            time.sleep(1)
            GPIO.output('P8_16', GPIO.HIGH)
            GPIO.cleanup()
            time.sleep(1)
    return lastSwitch


if __name__ == '__main__':
    print 'GPIO - Setting up...'
    GPIO.setup('P8_12', GPIO.IN) # S_FE
    GPIO.setup('P8_14', GPIO.IN) # S_AB
    
    GPIO.setup('P8_16', GPIO.OUT)
    GPIO.output('P8_16', GPIO.HIGH)
    GPIO.cleanup()

    
    print 'Actuator - Initializing...'
    actualState = UNKNOWN
    failedAttempts = 0
    lastSwitch = 0
    lastCheck = 0
    lastSuccess = 0
    try:
        fExpectedState = open(base_path + '/expectedState', 'rb', buffering=0)
    except IOError:
        print 'IOError'
        # TODO: adicionar erro aos registros
        # TODO: tentar novamente

    while(1):
        if GPIO.input('P8_12') == 0 and GPIO.input('P8_14') == 0:
            if(actualState != UNKNOWN):
                p = Person(type='fus', desc=u'Erro - Estado impossível')
                p.save()
            actualState = UNKNOWN
        if GPIO.input('P8_12') == 0 and GPIO.input('P8_14') == 1:
            if(actualState != CLOSED):
                p = Person(type='evf', desc=u'Evento - Vagão fechou')
                p.save()
            actualState = CLOSED
        if GPIO.input('P8_12') == 1 and GPIO.input('P8_14') == 0:
            if(actualState != OPEN):
                p = Person(type='eva', desc=u'Evento - Vagão abriu')
                p.save()
            actualState = OPEN
        if GPIO.input('P8_12') == 1 and GPIO.input('P8_14') == 1:
            if(actualState != MOVING):
                p = Person(type='avm', desc=u'Alerta - Vagão se movendo')
                p.save()
            actualState = MOVING
        
        # TODO: verificar porque na primeira execucao ele nao considera o sensor
        # Check if actualState matches expectedState
        now = time.time()
        if(abs(now - lastCheck) >= 0.5):
            lastCheck = now
            try:
                fExpectedState.seek(0)
                expectedState = fExpectedState.read(1)
                if (ord(expectedState) & STATE_MANUAL_OPEN): # Manual open mode
                    if(actualState != OPEN and actualState != UNKNOWN):
                        lastSwitch = switch(OPEN, lastSwitch)
                elif(ord(expectedState) & (STATE_RAINING | STATE_NIGHT | STATE_MANUAL_CLOSED | STATE_FAIL)): # Rain, night, manual closed mode or system failed
                    if(actualState != CLOSED and actualState != UNKNOWN):
                        lastSwitch = switch(CLOSED, lastSwitch)
                else: # Must be open
                    if(actualState != OPEN and actualState != UNKNOWN):
                        lastSwitch = switch(OPEN, lastSwitch)
                lastSuccess = now
            except Exception as ex:
                print 'Actuator - Exception'
                print str(ex)
                
            if(abs(now - lastSuccess) >= 2.5): # Control failed -> CLOSE
                if(actualState != CLOSED):
                    lastSwitch = switch(CLOSED, lastSwitch)
        # Sleep a while
        time.sleep(0.05)
