# -*- coding: utf-8 -*-

def check_rain():
    global lastRain
    global newState
    global stateChanged
    global timeoutCounter
    global exceptionCounter
    
    print 'check_rain()'
    try:
        r = requests.get('http://www.sel.eesc.usp.br/liosensors/json_data.php', timeout=5)
        #r = requests.get('http://192.168.1.111:8008/json_data', timeout=5)
        j = r.json()
        #rain = j['sensors'][0]['lastRUnix']
        #now = j['sensors'][0]['timeUnix']
        
        rain = j['lastRUnix']
        now = j['timeUnix']
        if ((newState & STATE_RAINING == STATE_OPEN) and (rain != lastRain)): # It's raining
            stateChanged = True
            lastState = newState
            newState = lastState | STATE_RAINING
            
            p = Person(type='ers', desc='Evento - Chuva detectada')
            p.save()
            lastRain = rain
        
        if ((newState & STATE_RAINING != STATE_OPEN) and (rain != lastRain)): # It's still raining
            lastRain = rain
        
        if((newState & STATE_RAINING) and abs(now - lastRain) > 150): # Rain end
            stateChanged = True
            lastState = newState
            newState = lastState & (~STATE_RAINING)
            p = Person(type='ere', desc=u'Evento - Término de chuva detectado')
            p.save()
            
        exceptionCounter = 0
        # TODO: testar se a conexão reestabelecida funciona
        if (newState & STATE_FAIL):
            stateChanged = True
            lastState = newState
            newState = lastState & (~STATE_FAIL)
            
            p = Person(type = 'avc', desc = u'Alerta - Acesso aos dados dos sensores reestabelecido')
            p.save()
    except Exception as ex:
        template = 'An exception of type {0} occurred. Arguments:\n{1!r}'
        message = template.format(type(ex).__name__, ex.args)
        print message
        print 'check_rain(): Exception'
        if(type(ex).__name__ == 'ConnectTimeout'):
            timeoutCounter = timeoutCounter + 1
            
        #if(type(ex).__name__ == 'ConnectionError'):
        #    print 'Conexao negada, iniciando sleep'
        #    time.sleep(2)
        #    print 'Sleep completo, voltando a exec normal'
            
        exceptionCounter = exceptionCounter + 1
        


        if(exceptionCounter >= 12):
            print 'muitos erros consecutivos, fechar vagao'
            if (exceptionCounter == 12):
                stateChanged = True
                lastState = newState
                newState = lastState | STATE_FAIL
            
                p = Person(type = 'fvc', desc = u'Erro - Várias tentativas falhas de acessar os dados dos sensores')
                p.save()
       

import sys
import os
import time
import django
import requests
from django.utils import timezone
from astral import Location

base_path = os.path.dirname(os.path.abspath(__file__))
sys.path.append(base_path + '/../liocontrol')
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'liocontrol.settings')
django.setup()

from panel.models import Person
from datetime import datetime

timeoutCounter = 0
exceptionCounter = 0
STATE_OPEN          = 0b00000000
STATE_RAINING       = 0b00000001
STATE_NIGHT         = 0b00000010
STATE_MANUAL_CLOSED = 0b00000100
STATE_MANUAL_OPEN   = 0b00001000
STATE_FAIL          = 0b10000000
#TODO: começar a usar a mascara nos if's
STATE_MASK_CLOSE    = 0b10001111

MODE_AUTO           = 0b00000000
MODE_MANUAL_OPEN    = 0b10000000
MODE_MANUAL_CLOSED  = 0b10000001
MODE_NULL           = 0b11111111

try:

    r = requests.get('http://www.sel.eesc.usp.br/liosensors/json_data.php', timeout=5)
    #r = requests.get('http://192.168.1.111:8008/json_data', timeout=5)
    j = r.json()
    #lastRain = j['sensors'][0]['lastRUnix']
    lastRain = j['lastRUnix']
except:
    lastRain = 0
    
print lastRain


fMode = open(base_path + '/mode', 'r', buffering=0)
fExpectedState = open(base_path + '/expectedState', 'r+')
newState = ord(fExpectedState.read(1))      
fExpectedState.close() 

print newState

stateChanged=False
oldMode = MODE_NULL
 
l = Location()
l.name = 'Sao Carlos'
l.region = 'Brasil'
l.latitude = -22.006507
l.longitude = -47.897846
l.timezone = 'Brazil/East'
l.elevation = 835

while(1):
    fMode.seek(0)
    mode = ord(fMode.read(1))
    
    if(mode == MODE_AUTO):
        print 'MODE_AUTO'
        oldMode = mode
        if(newState & STATE_MANUAL_CLOSED):
            lastState = newState
            newState = lastState & (~STATE_MANUAL_CLOSED)
            stateChanged = True
        if(newState & STATE_MANUAL_OPEN):
            lastState = newState
            newState = lastState & (~STATE_MANUAL_OPEN)
            stateChanged = True
        if(newState & STATE_NIGHT):
            sunrise = l.sunrise(local=False)
            sunset = l.sunset(local=False)
            now = timezone.now()
            if(sunrise < now < sunset): # Sunrise
                stateChanged = True
                lastState = newState
                newState = lastState & (~STATE_NIGHT)
                p = Person(type='esr', desc='Evento - Nascer do sol')
                p.save()
                check_rain()
            else:
                check_rain()
                
        else:
            sunset = l.sunset(local=False)
            now = timezone.now()
            if(now > sunset): # Sunset
                stateChanged = True
                lastState = newState
                newState = lastState | STATE_NIGHT
                p = Person(type='ess', desc=u'Evento - Pôr do sol')
                p.save()
            else:
                check_rain()

        # write to file 
        # TODO: caminho relativo para o arquivo
        if(stateChanged):
            print 'stateChanged, newState: ' + str(newState)
            fExpectedState = open(base_path + '/expectedState', 'r+')
            fExpectedState.write(chr(newState))
            fExpectedState.close()
            stateChanged = False
    elif(mode == MODE_MANUAL_OPEN):
        print 'MODE_MANUAL_OPEN'
        if(mode != oldMode):
            print 'mode != oldMode'           
            fExpectedState = open(base_path + '/expectedState', 'r+')
            lastState = fExpectedState.read(1)
            newState = ord(lastState) & (~STATE_MANUAL_CLOSED)
            newState = newState | STATE_MANUAL_OPEN
            fExpectedState.seek(0)
            fExpectedState.write(chr(newState))
            fExpectedState.close()
            oldMode = mode
    elif(mode == MODE_MANUAL_CLOSED):
        print 'MODE_MANUAL_CLOSED'
        if(mode != oldMode):
            print 'mode != oldMode'
            fExpectedState = open(base_path + '/expectedState', 'r+')
            lastState = fExpectedState.read(1)
            newState = ord(lastState) & (~STATE_MANUAL_OPEN)
            newState = newState | STATE_MANUAL_CLOSED
            fExpectedState.seek(0)
            fExpectedState.write(chr(newState))
            fExpectedState.close()
            oldMode = mode

    print 'timeoutCounter: ' + str(timeoutCounter)
    print 'exceptionCounter: ' + str(exceptionCounter)
    time.sleep(1)
             

