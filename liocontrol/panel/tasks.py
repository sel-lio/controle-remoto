# Create your tasks here
from __future__ import absolute_import, unicode_literals
from celery import shared_task
from liocontrol.celery import app
from celery import Celery
from celery.schedules import crontab
from celery.schedules import solar
from .models import Person
import Adafruit_BBIO.GPIO as GPIO
import logging
import time
import requests

STATE_OPEN =            0b00000000
STATE_RAINING =         0b00000001
STATE_NIGHT =           0b00000010
STATE_MANUAL_OPEN =     0b10000000
STATE_MANUAL_CLOSED =   0b10000011

logger = logging.getLogger('panel')
fhandler = logging.FileHandler('/home/lio/log/panel.log')
formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
fhandler.setFormatter(formatter)
logger.addHandler(fhandler)
logger.setLevel(logging.INFO)
lastRain = 0
lastState = STATE_OPEN

app.conf.beat_schedule = {
    'add-every-monday-morning': {
        'task': 'panel.tasks.beat',
        'schedule': 600.0,
    },
    'check-rain': {
        'task': 'panel.tasks.check_rain',
        'schedule': 10.0,
    },
    'sunrise': {
        'task': 'panel.tasks.sunrise',
        'schedule': solar('sunrise', -22.006507, -47.897846),
    },
    'sunset': {
        'task': 'panel.tasks.sunset',
        'schedule': solar('sunset', -22.006507, -47.897846),
    },
}

@shared_task
def add(x, y):
    return x + y

@shared_task
def sunrise():
# TODO: caminho relativo para o arquivo
    fExpectedState = open("/home/lio/Projects/controle-remoto/control/expectedState", "r+")
#TODO: Manter estado anterior (com or e and)
    state = int(ord(fExpectedState.read(1)))
    newState = state & (~STATE_NIGHT)
    fExpectedState.seek(0)
    fExpectedState.write(chr(newState))
    fExpectedState.close()
    p1 = Person(type="c", desc="Sunrise - lastState: " + str(state) + " newState: " + str(newState))
    p1.save()
    logger.info('Sunrise - OPEN')

@shared_task
def sunset():
# TODO: caminho relativo para o arquivo
    fExpectedState = open("/home/lio/Projects/controle-remoto/control/expectedState", "r+")
#TODO: Manter estado anterior (com or e and)
    state = int(ord(fExpectedState.read(1)))
    newState = state | STATE_NIGHT
    fExpectedState.seek(0)
    fExpectedState.write(chr(newState))
    fExpectedState.close()
    p1 = Person(type="b", desc="Sunset - lastState: " + str(state) + " newState: " + str(newState))
    p1.save()
    logger.info('Sunset - CLOSE')

@shared_task
def beat():
    p = Person(type="a", desc="Beat")
    p.save()
    logger.info('BEAT')

@shared_task
def check_rain():
# TODO: verificar possiveis problemas de concorrencia
    global lastRain
    global lastState
    try:
        r = requests.get("http://143.107.235.2:8008/json_data")
        j = r.json()
        rain = j["sensors"][0]["lastRUnix"]
        now = j["sensors"][0]["timeUnix"]
        if ((lastState & STATE_RAINING == STATE_OPEN) and (rain != lastRain)): # It's raining
            lastRain = rain
            lastState = STATE_RAINING
# TODO: caminho relativo para o arquivo
            fExpectedState = open("/home/lio/Projects/controle-remoto/control/expectedState", "r+")
            state = int(ord(fExpectedState.read(1)))
            newState = state | STATE_RAINING    # Adds raining flag
            fExpectedState.seek(0)
            fExpectedState.write(chr(newState))
            fExpectedState.close()
            p = Person(type="f", desc="AUTO - check_rain: Chuva detectada! - lastRain: " + str(lastRain) + " rain: " + str(rain))
            p.save()
        
        if((lastState & STATE_RAINING) and abs(now - lastRain) > 60):
            lastState = STATE_OPEN
# TODO: caminho relativo para o arquivo
            fExpectedState = open("/home/lio/Projects/controle-remoto/control/expectedState", "r+")
            state = int(ord(fExpectedState.read(1)))
            newState = state & (~STATE_RAINING)   # Removes raining flag
            fExpectedState.seek(0)
            fExpectedState.write(chr(newState))
            fExpectedState.close()
            p = Person(type="f", desc="AUTO - check_rain: Termino de chuva detecado! - lastRain: " + str(lastRain) + " now: " + str(now))
            p.save()
            
    except IOError:
    #TODO: maxAttempts failed -> close!
        print "check_rain(): IOError"
        #p = Person(type="fe", desc="(ERROR) AUTO - check_rain: IOError")
        #p.save()
    except Exception as ex:
        template = "An exception of type {0} occurred. Arguments:\n{1!r}"
        message = template.format(type(ex).__name__, ex.args)
        print message
        print "check_rain(): Exception"
        #p = Person(type="fe", desc="(ERROR) AUTO - check_rain: Exception")
        #p.save()
 
@shared_task
def mul(x, y):
    return x * y


@shared_task
def xsum(numbers):
    return sum(numbers)
