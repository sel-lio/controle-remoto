from django.conf.urls import url
from django.contrib.auth import views as auth_views

from . import views

app_name = 'liocontrol'

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^config/$', views.config, name='config'),
    url(r'^manual/$', views.manual, name='manual'),
    url(r'^data/$', views.data, name='data'),
    url(r'^camera/$', views.camera, name='camera'),
    url(r'^logs/$', views.logs, name='logs'),
    url(r'^logs/download/$', views.logs_download, name='logs_download'),
    url(r'^close/$', views.manual_close, name='close'),
    url(r'^open/$', views.manual_open, name='open'),
    url(r'^sethigh/$', views.sethigh, name='sethigh'),
    url(r'^setlow/$', views.setlow, name='setlow'),
    url(r'^login/$', views.user_login, name='login'),
    url(r'^logout/$', auth_views.logout, {'template_name': 'auth/logged_out.html'}, name='logout'),
    
]
