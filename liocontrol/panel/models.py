# -*- coding: utf-8 -*-
from django.db import models
from django.utils import timezone

class Person(models.Model):
    type = models.CharField(max_length=3)
    desc = models.CharField(max_length=200)
    # TODO: começar a armazenar when como unix time
    when = models.DateTimeField(default=timezone.now)
    #when = models.DateTimeField(auto_now_add=True)
	
class Config(models.Model):
	long = models.DecimalField(max_digits=9,decimal_places=6)
	lati = models.DecimalField(max_digits=8,decimal_places=6)
