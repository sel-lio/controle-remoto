# -*- coding: utf-8 -*-

from django.shortcuts import render
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.http import HttpResponseBadRequest
from django.http import JsonResponse

from panel import settings
from .models import Config, Person
from decimal import *
import Adafruit_BBIO.GPIO as GPIO
import time as time_t
from django.shortcuts import redirect
from django.urls import reverse
from django.contrib.auth.views import login
from datetime import timedelta
from datetime import time
from datetime import datetime
from time import mktime
from collections import OrderedDict

from astral import Astral
from astral import Location
import urllib
import re
import json
import requests
import csv

l = Location()
l.name = "Sao Carlos"
l.region = "Brasil"
l.latitude = -22.006507
l.longitude = -47.897846
l.timezone = "Brazil/East"
l.elevation = 835

STATE_OPEN =            0b00000000
STATE_CLOSED =          0b11111111
STATE_RAINING =         0b00000001
STATE_NIGHT =           0b00000010
STATE_MANUAL_OPEN =     0b10000000
STATE_MANUAL_CLOSED =   0b10000011

MODE_AUTO           = 0b00000000
MODE_MANUAL_OPEN    = 0b10000000
MODE_MANUAL_CLOSED  = 0b10000001

def index(request):
    if request.user.is_authenticated():
        # Get sunrise and sunset times
        sunrise = l.sunrise().strftime("%H:%M:%S")
        sunset = l.sunset().strftime("%H:%M:%S")
        # Load sensors data
        iuv = 0
        data_hora = 'default'
        try:
            #r = requests.get("http://192.168.1.111:8008/json_data", timeout=5)
            
            r = requests.get('http://www.sel.eesc.usp.br/liosensors/json_data.php', timeout=5)
            data = r.json()
            #iuv = str(data['sensors'][0]['lastUV'])
            iuv = str(data['lastUV'])
            iuv = float(iuv)
            iuv = round(iuv)
            iuv = int(iuv)
            iuv = str(iuv)
            #data_hora = data['sensors'][0]['time']
            data_hora = data['time']
        except Exception as ex:
            iuv = 0
            data_hora = 'Falha ao consultar dados do IUV'
            print 'Exception'
        #htmlfile=urllib.urlopen("http://192.168.1.111:8008/json_data")
        #htmltext=htmlfile.read()
        #data = json.loads(htmltext)
        # Get UVI and sensors timestamp
        
        # Get mode and expected state
        fMode = open("/home/lio/controle-remoto/control/mode", "r")
        fExpectedState = open("/home/lio/controle-remoto/control/expectedState", "rb")

        mode = ord(fMode.read(1))
        expectedState = int(ord(fExpectedState.read(1)))
        
        fMode.close()
        fExpectedState.close()
        
        manual = False
        night = False
        rain = False
        if(mode != MODE_AUTO):
            manual = True
        elif(expectedState & STATE_NIGHT != STATE_OPEN):
            night = True
        elif(expectedState & STATE_RAINING != STATE_OPEN):
            rain = True
            
        state = "Pânico"
        stateColor = "#FF0000"
        fActualState = open("/home/lio/controle-remoto/control/actualState", "rb")
        actualState = int(ord(fActualState.read(1)))
        fActualState.close()
        if(actualState == STATE_OPEN):
            state = "Aberto"
            stateColor = "#00FF00"
        elif(actualState == STATE_CLOSED):
            state = "Fechado"
            stateColor = "#00FF00"
        # Get system time
        now = (datetime.now()+timedelta(minutes=2)).strftime("%d-%m-%Y %H:%M:%S");

        return render(request, "panel/index.html", {'now': now, 'iuv_data_hora': data_hora, 'state': state, 'state_color': stateColor, 'rain':rain, 'night':night, 'iuv': iuv, 'sunrise': sunrise, 'sunset': sunset, 'manual': manual})
    else:
        return HttpResponseRedirect(reverse('liocontrol:login'))


def config(request):
    if request.user.is_authenticated():
        if request.method == "POST":
        # create a form instance and populate it with data from the request:
            longitude = Decimal(request.POST.get("longitude")).quantize(Decimal('.000001'), rounding=ROUND_DOWN)
            latitude = Decimal(request.POST.get("latitude")).quantize(Decimal('.000001'), rounding=ROUND_DOWN)
            print longitude
            if (longitude > 180.0 or longitude < -180.0 or latitude > 90.0 or latitude < -90.0):
                return HttpResponse("Error")
            else:
                config = Config.objects.get(pk=1)
                config.long = longitude
                config.lati = latitude
                config.save()
                return render(request, "panel/config.html", {'config': config})
                
        
        try:
            config = Config.objects.get(pk=1)
        except Config.DoesNotExist:
            return HttpResponse("Config does not exist")
        return render(request, "panel/config.html", {'config': config})
    else:
        return HttpResponseRedirect(reverse('liocontrol:login'))

def manual(request):
    if request.user.is_authenticated():
        if request.method == "POST":
            mode = request.POST.get("mode")            
            try:
                fMode = open("/home/lio/controle-remoto/control/mode", "r+")
                actualMode = ord(fMode.read(1))
                fMode.seek(0)
                if(mode == 'auto' and actualMode != MODE_AUTO):
                    fMode.write(chr(MODE_AUTO))
                    p = Person(type="ma", desc="Modo alterado para automático.")
                    p.save()
                elif(mode == 'manual_open' and actualMode != MODE_MANUAL_OPEN):
                    fMode.write(chr(MODE_MANUAL_OPEN))
                    p = Person(type="mmo", desc="Modo alterado para manual aberto.")
                    p.save()
                elif(mode == 'manual_closed' and actualMode != MODE_MANUAL_CLOSED):
                    fMode.write(chr(MODE_MANUAL_CLOSED))
                    p = Person(type="mmc", desc="Modo alterado para manual fechado.")
                    p.save()
                else:
                    p = Person(type="wm", desc="WARNING: O modo solicitado não é válido.")
                    p.save()
                    fMode.close()
                    return HttpResponseBadRequest()          
                
                fMode.close()
                return HttpResponse()
            except IOError:
                p = Person(type="em", desc="ERROR (IOError): Falha ao alterar o modo.")
                p.save()
                
                return HttpResponseBadRequest("ERROR (IOError): Falha ao alterar o modo.")
        else:
            try:
                fMode = open("/home/lio/controle-remoto/control/mode", "r")
                mode = ord(fMode.read(1))
                fMode.close()
                
                manual = False
                manual_closed = False
                print mode
                print MODE_MANUAL_OPEN
                if(mode == MODE_MANUAL_OPEN):
                    manual = True
                elif(mode == MODE_MANUAL_CLOSED):
                    manual = True
                    manual_closed = True
                
                return render(request, "panel/manual.html", {'manual': manual, 'manual_closed': manual_closed})
            except IOError:
                p = Person(type="em", desc="ERROR (IOError): Falha ao consultar o modo.")
                p.save()
                
                return HttpResponseBadRequest("ERROR (IOError): Falha ao consultar o modo.")
    else:
        return HttpResponseRedirect(reverse('liocontrol:login'))
    
def data(request):
    # Uptime
    with open('/proc/uptime', 'r') as f:
            uptime_seconds = float(f.readline().split()[0])
            t = timedelta(seconds = uptime_seconds)
            hours = t.seconds//3600
            minutes = (t.seconds//60)%60
            seconds = t.seconds%60
            uptime_string = str(t.days) + ' dia(s), ' + str(hours).zfill(2) + ':' + str(minutes).zfill(2) + ':' + str(seconds).zfill(2)
       
    return HttpResponseRedirect(reverse('liocontrol:login'))

def camera(request):
    return render(request, "panel/camera.html")

def logs(request):
    if request.user.is_authenticated():
        time = int(request.GET.get('time', '1'))
        type = int(request.GET.get('type', '1'))
        type_chr = ''
        if (type == 2):
            type_chr = 'e'
        if (type == 3):
            type_chr = 'a'
        if (type == 4):
            type_chr = 'f'
        objects_ftime = Person.objects.filter(when__lte=datetime.today(), when__gt=datetime.today()-timedelta(hours=24*time))
        objects = objects_ftime.filter(type__startswith=type_chr)
        return render(request, "panel/log.html", {'object_list': objects, 'current_path': request.get_full_path(), 'type': type, 'time': time})
    else:
        return HttpResponseRedirect(reverse('liocontrol:login'))

def logs_download(request):
    if request.user.is_authenticated():
        time = int(request.GET.get('time', '1'))
        type = int(request.GET.get('type', '1'))
        type_chr = ''
        if (type == 2):
            type_chr = 'e'
        if (type == 3):
            type_chr = 'a'
        if (type == 4):
            type_chr = 'f'
        objects_ftime = Person.objects.filter(when__lte=datetime.today(), when__gt=datetime.today()-timedelta(hours=24*time))
        objects = objects_ftime.filter(type__startswith=type_chr)
        
        # Create the HttpResponse object with the appropriate CSV header.
        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename="logs.csv"'
        
        writer = csv.writer(response)
        writer.writerow(['Data e Hora', 'Tipo', u'Descrição'.encode('utf-8')])
        for o in objects:
            # TODO: adicionar tempo UNIX no CSV [int(mktime(o.when.timetuple())), mas antes precisa corrigir no models.py
            writer.writerow([o.when.strftime("%d/%m/%Y %H:%M:%S"), o.type, o.desc.encode('utf-8')])

        return response    
    else:
        return HttpResponseRedirect(reverse('liocontrol:login'))
        

def manual_close(request):
    try:
# TODO: caminho relativo para o arquivo
        fExpectedState = open("/home/lio/controle-remoto/control/expectedState", "w")
        fExpectedState.write(chr(STATE_MANUAL_CLOSED))
        fExpectedState.close()
        p = Person(type="d", desc="MANUAL - Close")
        p.save()
    except IOError:
        print "close(): IOError"
        p = Person(type="de", desc="(ERROR) MANUAL - Close: IOError")
    return redirect('http://143.107.235.2:8000/panel/')
    
def manual_open(request):
    try:
# TODO: caminho relativo para o arquivo
        fExpectedState = open("/home/lio/controle-remoto/control/expectedState", "w")
        fExpectedState.write(chr(STATE_OPEN))
        fExpectedState.close()
        p = Person(type="e", desc="MANUAL - Open")
        p.save()
    except IOError:
        print "close(): IOError"
        p = Person(type="ee", desc="(ERROR) MANUAL - Open: IOError")
        
        
    return redirect('http://143.107.235.2:8000/panel/')

def sethigh(request):
    GPIO.setup("GPIO1_14", GPIO.OUT)
    GPIO.output("GPIO1_14", GPIO.HIGH)
    GPIO.cleanup()
    time_t.sleep(0.5)
    GPIO.output("GPIO1_14", GPIO.LOW)
    GPIO.cleanup()
    return HttpResponse()

def setlow(request):
    p = Person.objects.filter(createdate__lte=datetime.datetime.today(), createdate__gt=datetime.datetime.today())
    with open('/proc/uptime', 'r') as f:
        uptime_seconds = float(f.readline().split()[0])
        t = timedelta(seconds = uptime_seconds)
        hours = t.seconds//3600
        minutes = (t.seconds//60)%60
        seconds = t.seconds%60
        uptime_string = str(t.days) + ' dia(s), ' + str(hours).zfill(2) + ':' + str(minutes).zfill(2) + ':' + str(seconds).zfill(2)
    return HttpResponse(uptime_string)
        
        

def user_login(request):
    if request.user.is_authenticated():
        return HttpResponseRedirect(reverse('liocontrol:index'))
    else:
        return login(request, template_name='auth/login.html')


def handler404(request):
    return redirect('index', permanent=False)
