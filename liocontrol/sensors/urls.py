from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.index, name='sensors-index'),
    url(r'^data/$', views.data, name='sensors-data'),
]