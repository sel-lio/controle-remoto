from django.shortcuts import render
from django.http import HttpResponse
from django.http import JsonResponse
from collections import OrderedDict
from datetime import datetime
from time import mktime
from astral import Location

import Adafruit_BBIO.GPIO as GPIO

def index(request):
    # TODO: Fazer pagina web com os dados dos sensores
    return HttpResponse("You're at the sensors index.")
    
def data(request):
    l = Location()
    l.name = 'Sao Carlos'
    l.region = 'Brasil'
    l.latitude = -22.006507
    l.longitude = -47.897846
    l.timezone = 'Brazil/East'
    l.elevation = 835
    
    sunrise_local = l.sunrise(local=True).strftime("%d/%m/%Y %H:%M:%S")
    sunset_local = l.sunset(local=True).strftime("%d/%m/%Y %H:%M:%S")
    sunrise_utc = l.sunrise(local=False).strftime("%d/%m/%Y %H:%M:%S")
    sunset_utc = l.sunset(local=False).strftime("%d/%m/%Y %H:%M:%S")
    
    # TODO: corrigir permissoes de GPIO 
    #GPIO.setup("GPIO1_12", GPIO.IN)
    GPIO.setup("P8_12", GPIO.IN)
    #GPIO.setup("GPIO0_26", GPIO.IN)
    GPIO.setup("P8_14", GPIO.IN)
    GPIO.cleanup()
    
    closed = not bool(GPIO.input("P8_12"))
    open = not bool(GPIO.input("P8_14"))
    
    # TODO: add timezone info
    dt_now = datetime.now()
    time = dt_now.strftime("%d/%m/%Y %H:%M:%S") 
    time_utc = datetime.utcnow().strftime("%d/%m/%Y %H:%M:%S") 
    time_unix = mktime(dt_now.timetuple())
    last_ts = 0
    rain = False
    rain_lvl = 0
    last_rain = 0
    last_rain_unix = 0
    last_temp = 0
    last_hum = 0
    last_dust = 0
    last_uv = 0
    last_uv_volt = 0
    
    sensors = OrderedDict(
    [   ('local', {
        'time': time,
        'sunrise': sunrise_local,
        'sunset': sunset_local }),
        ('UTC', {
        'time': time_utc,
        'sunrise': sunrise_utc,
        'sunset': sunset_utc }),
        ('timeUnix', time_unix),
        ('lastTS', last_ts),
		('rain', rain),
		('rainLevel', rain_lvl),
        ('lastRain', last_rain),
		('lastRainUnix', last_rain_unix),
		('lastTemp',last_temp),
		('lastHum', last_hum),
		('lastDust', last_dust),
		('lastUV', last_uv),
		('lastVolt', last_uv_volt),
		('open', open),
		('closed', closed)
	])
    
    return JsonResponse({'sensors': {0: sensors}})

