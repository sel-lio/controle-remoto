#!/usr/bin/python
import MySQLdb
import time
from sqmodule import query_last, query_last_UV, query_last_UVsg, create_table_UVsg

class sqlUVI(object):
	
	def __init__(self):
		self.db = MySQLdb.connect("143.107.182.35","liouv","LeUvLi75","liouv" )

	def __del__(self):
		self.db.close()
	
	def selectAll(self):
		query = self.db.cursor()
		query.execute("SELECT * FROM IUV")
		return query.fetchall()
		query.close()
		
	def selectDate(self, date):
		query = self.db.cursor()
		query.execute("SELECT * FROM IUV WHERE date LIKE \'%s\'"%(date) )
		return query.fetchall()
		query.close()

	def selectDateTime(self, date, time):
		query = self.db.cursor()
		query.execute("SELECT * FROM IUV WHERE date LIKE \'%s\' AND time LIKE \'%s\'"%(date,time)) 
		return query.fetchall()
		query.close()

	def insert(self, date, time, voltage, uv_index):
		query = self.db.cursor()
		query.execute("INSERT INTO IUV (date, time, voltage, uv_index) VALUES (\'%s\', \'%s\', \'%d\', \'%d\')"%(date, time, voltage, uv_index))
		self.db.commit()
		query.close()

	def insertRain(self,date,time,rain,temp,hum,dust):
		query = self.db.cursor()
		query.execute("INSERT INTO LOGRAIN (DATE, TIME, RAIN, TEMP, HUM, DUST) VALUES (\'%s\', \'%s\', \'%d\', \'%f\', \'%f\', \'%f\')"%(date, time, rain, temp, hum, dust))
		self.db.commit()
		query.close()

	def insertSkye(self,date,time,adc,volt,uvi):
		query = self.db.cursor()
		query.execute("INSERT INTO UVskye (DATE, TIME, ADC, VOLT, UVI) VALUES (\'%s\', \'%s\', \'%d\', \'%f\', \'%f\')"%(date, time, adc, volt, uvi))
		self.db.commit()
		query.close()
		
	def deleteAll(self):
		query = self.db.cursor()
		query.execute("DELETE FROM IUV")
		self.db.commit()
		query.close()
		
	def insertTest(self):
		self.insert("27/05/2014", "20:51:00", 23, 2)
		self.insert("27/05/2014", "20:52:00", 24, 2)
		self.insert("27/05/2014", "20:53:00", 24, 3)
		self.insert("27/05/2014", "20:54:00", 23, 4)
		self.insert("27/05/2014", "20:55:00", 25, 2)
		self.insert("27/05/2014", "20:56:00", 23, 5)
		self.insert("27/05/2014", "20:57:00", 22, 2)
		self.insert("27/05/2014", "20:58:00", 23, 5)
		self.insert("27/05/2014", "20:59:00", 245, 2)
		self.insert("26/05/2014", "20:59:00", 34, 5)

	def createTableSkye(self):
		query = self.db.cursor()
		query.execute('''CREATE TABLE UVskye
		   (ID int NOT NULL AUTO_INCREMENT,
		   	DATE		   TEXT		NOT NULL,
			TIME		   TEXT 	NOT NULL,
			ADC            INT   	NOT NULL,
		    VOLT           REAL     NOT NULL,
		    UVI       	   REAL		NOT NULL,
		    PRIMARY KEY (ID));''')

		self.db.commit()
		query.close()

	def createTableRain(self):
		query = self.db.cursor()
		query.execute('''CREATE TABLE LOGRAIN
		   (ID int NOT NULL AUTO_INCREMENT,
		   	DATE		   TEXT		NOT NULL,
			TIME		   TEXT 	NOT NULL,
			RAIN           INT   	NOT NULL,
		    TEMP           REAL     NOT NULL,
		    HUM       	   REAL		NOT NULL,
		    DUST       	   REAL		NOT NULL,
		    PRIMARY KEY (ID));''')

		self.db.commit()
		query.close()


if __name__ ==  "__main__":
	sql = sqlUVI()

	while(True):
		last_row = query_last('rainlog.sqlite','log')
		date = last_row[0]
		timeS = last_row[1]
		rain = last_row[2]
		temp = last_row[3]
		hum  = last_row[4]
		dust = last_row[5]

		#print last_row
		#print date, timeS, rain, temp, hum, dust
		try:
			sql.insertRain(date,timeS,rain,temp,hum,dust)
			print "sucessfully saved RAIN @ SEL"
		except:
			print "cant save RAIN @ SEL"
			sql=sqlUVI()
			pass

		last_row_uv = query_last_UV('rainlog.sqlite','logUV') # for skye sensor

		date = last_row_uv[0]
		timeS = last_row_uv[1]
		adc = last_row_uv[2]
		volt = last_row_uv[3]
		uvi  = last_row_uv[4]

		try:
			sql.insertSkye(date,timeS,adc,volt,uvi)
			print "sucessfully saved SKYE @ SEL"
		except:
			print "cant save SKYE @ SEL"
			sql=sqlUVI()
			pass

		last_row_uv_sg = query_last_UVsg('rainlog.sqlite','loguvsg') # for sglux sensor

		date = last_row_uv[0]
		timeS = last_row_uv[1]
		adc = last_row_uv[2]
		volt = last_row_uv[3]
		uvi  = last_row_uv[4]

		
		try:
			sql.insert(date,timeS,volt,uvi)
			print "sucessfully saved SGLUX @ SEL"
		except:
			print "cant save SGLUX @ SEL"
			sql=sqlUVI()
			pass


# SGLUX
        
#		last_row_uvsg = query_last_UVsg('rainlog.sqlite','logUVsg') # for sglux sensor
		
#		date = last_row_uv[0]
#		timeS = last_row_uv[1]
#		adc = last_row_uv[2]
#		volt = last_row_uv[3]
#		uvi  = int(last_row_uv[4])

#		try:
#			sql.insert(date,timeS,adc,uvi)
#			print "sucessfully saved SGLUX @ SEL"
#		except:
#			print "cant save SGLUX @ SEL"
#			pass


		time.sleep(5)
	
	#sql.insertTest()
	#sql.insert("27/05/2014","20:55:00", 34, 23)
	
	# for row in sql.selectDateTime("27/05/2014","20:55:00"):
	# 	print row
	#sql.createTableSkye()
	#sql.createTableRain()
	
