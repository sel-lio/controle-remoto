#coding: utf-8

import urllib2
import time
import sched, time
import sys, os
from   sqmodule import insert_row_UVinpe, create_table_UVinpe


s = sched.scheduler(time.time, time.sleep)
tdelay = 900   #900 sec or 15 minutes

def do(sc): 
    print "Checking and Saving INPE data..."
    # do your stuff
    print "INPE\t\t" + getINPE()
    saveINPE()
    s.enter(tdelay, 1, do, (sc,))

def month_converter(month):
	return{
			'Jan' : "01",
			'Feb' : "02",
			'Mar' : "03",
			'Apr' : "04",
			'May' : "05",
			'Jun' : "06",
			'Jul' : "07",
			'Aug' : "08",
			'Sep' : "09", 
			'Oct' : "10",
			'Nov' : "11",
			'Dec' : "12"}[month]

def getINPE():
	try:
		response = urllib2.urlopen('http://servicos.cptec.inpe.br/XML/cidade/4774/condicoesAtuaisUV.xml')
		content =  str(response.read())
		
		data = content[content.find('<data>')+len('<data>'):content.find('</data>')]
		hora = content[content.find('<hora>')+len('<hora>'):content.find('</hora>')]
		iuv  = content[content.find('<iuv>')+len('<iuv>'):content.find('</iuv>')]
		
		return "%s - %s IUV %s"%(data,hora,iuv)
	except:
		return "Sem conexao com a internet"

def saveINPE():
	now = str(time.strftime("%c"))

	print now
	#day 	= now.split(" ")[2]
	day = str(time.strftime("%d"))
	print day
    #day		= str("%02d" %(int(day),))
	month 	= (now.split(" ")[1])
	month   = month_converter(month)
	year 	= now.split(" ")[4]
    
	base_path = os.path.dirname(os.path.abspath(__file__))
	newpath = '{0}/files/logsUV/INPE'.format(base_path)
	if not os.path.isdir(newpath): os.makedirs(newpath)

	inpelog = open("files/logsUV/INPE/inpeLog%s%s%s.txt"%(day,month,year), "a")
	inpelog.close()
	
	inpelog = open("files/logsUV/INPE/inpeLog%s%s%s.txt"%(day,month,year), "r+")
	
	try:
		
		 
		inpe_message = (getINPE()).split("-")[1]
		inpeUV = inpe_message.split(" ")[3]
		inpeTS = inpe_message.split(" ")[1]
		inpeTS = inpeTS.split("h")[0] +":"+ inpeTS.split("h")[1] +":00"
		#print inpeTS
		inpe_message = inpeTS +"\t"+ inpeUV
		

		nlines = 0
		for line in inpelog:
			nlines = nlines + 1
			pass

		if (nlines != 0):	
			last = line
			last = last[:(len(last)-2)]
		else:
			last = " "

		if (last!=inpe_message):
			#print inpe_message
			inpelog.write("%s\r\n" %inpe_message)
			print "updated INPE data"

			try:
				#dataRain = dataRain
				insert_row_UVinpe('rainlog.sqlite','loginpe',inpe_message)
			except:
				print "could not save sql INPE data"
				pass

	except:
		print "can't save or update INPE data"
		pass
			
	inpelog.close()

if __name__ == '__main__':
	print getINPE()
	saveINPE()

	s.enter(tdelay, 1, do, (s,))
	s.run()

