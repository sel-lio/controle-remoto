import serial
import datetime
import io
import os
    
from sqmodule import insert_row, insert_row_UV, insert_row_UVsg
#from sqUVI import insert, insertRain, insertSkye

class SerialHandler:
    ser = serial.Serial()

    def __init__(self, port):
        # Configure serial
        self.ser.port = port                    # Configure serial port name
        self.ser.baudrate = 9600                # Configure serial baudrate
        self.ser.bytesize = serial.EIGHTBITS    # Each byte has 8 bits
        self.ser.parity = serial.PARITY_NONE    # Disable parity check
        self.ser.stopbits = serial.STOPBITS_ONE # Set number of stop bits
        self.ser.xonxoff = False                # Disable software flow control
        self.ser.rtscts = False                 # Disable hardware (RTS/CTS) flow control
        self.ser.dsrdtr = False                 # Disable hardware (DSR/DTR) flow control
        self.ser.timeout = 10                    # Timeout for read
        self.ser.write_timeout = 10              # Timeout for write
        
        # Init serial
        try:
            self.ser.open()
        except serial.SerialException, e:
            print 'ERROR - Serial Exception: ' + str(e)
        
        self.ser.reset_input_buffer()
        self.ser.reset_output_buffer()
        
        self.sio = io.TextIOWrapper(io.BufferedRWPair(self.ser, self.ser,1))
        


    def getData(self, save = True):
        try:
            if self.ser.is_open == False:
                self.ser.open()
                if self.ser.is_open == False:
                    return
        except Exception as e:
            print str(e)
            print "could not open serial"
            return
        data_buffer = ""
        try:
            data_buffer = self.sio.readline()
            if 'Time out' in data_buffer:
                data_buffer = self.sio.readline()
        except Exception as e:
            print str(e)
            print "could not read serial data"
            self.ser.close()
            return
            
        print data_buffer
        data_split = data_buffer.split('\t')
        data_rain = data_split[0:5]
        data_rain = '\t'.join(data_rain)+'\r\n'
        #print data_rain

        data_uv = data_split[5:14]
        data_uv = '\t'.join(data_uv)
        #print data_uv
        
        #TODO: Checar se data_split eh dados ou um [u'Time out error,', u'\n']

        print data_split

        if data_split[0] != 'Time out error,':
            data = dict()
            data['water_level'] = data_split[0]
            data['dryer_state'] = data_split[1]
            data['temperature'] = data_split[2]
            data['humidity'] = data_split[3]
            data['dust_density'] = data_split[4]
            data['uvi'] = data_split[7]
            data['uvi_volt'] = data_split[6]
        
            #print data
            if save == True:
                self.saveData(data, data_rain, data_uv)
        
        
    def saveData(self, data, data_rain, data_uv):
        now_dt = datetime.datetime.now()
        now_string = now_dt.strftime("%d/%m/%Y %H:%M:%S")

        now_date = now_dt.strftime("%d_%m_%Y")
        now_time = now_dt.strftime("%H:%M:%S")

        base_path = os.path.dirname(os.path.abspath(__file__))

        path = '{0}/files/logs'.format(base_path)
        if not os.path.isdir(path): os.makedirs(path)
        path_uv = '{0}/files/logsUV'.format(base_path)
        if not os.path.isdir(path_uv): os.makedirs(path_uv)
        
        # Write to file
        try:
            fRainLog = open('%s/log-%s.txt'%(path,now_date), 'a')
            message = '%s\t%s'%(now_time,data_rain)
            
            print '%s/log-%s.txt'%(path,now_date)
            fRainLog.write(message)
            fRainLog.close()
            print "wrote file"
        except Exception as e:
            print "could not save log"
            print str(e)
            pass
            
        # Write to SQL
        try:
            message = '%s,%s\n'%(now_time,data_rain)
            insert_row('rainlog.sqlite','log',data)
        except Exception as e:
            print str(e)
            print "could not save sql data"
            pass


           
        # Write UV log
        try:
            fUVLog = open('%s/log-%s.txt'%(path_uv,now_date), 'a')
            print '%s/log-%s.txt'%(path_uv,now_date)
            message = '%s\t%s'%(now_time, data_uv)
            fUVLog.write(message)
            fUVLog.close()
            print "wrote uv file"
        except Exception as e:
            print str(e)
            print "could not save UV log"
            pass
            
            
        # Write UV to SQL
        try:
            insert_row_UV('rainlog.sqlite','loguv',data_uv)
            print "wrote skye to sqlite"
        except Exception as e:
            print str(e)
            print "could not save UV sql data"
            pass	

        # Write UV sglux to SQL
        try:
        #print dataUV
            insert_row_UVsg('rainlog.sqlite','loguvsg',data_uv)
            print "wrote sglux to sqlite"
        except Exception as e:
            print str(e)
            print "could not save UV SG sql data"
            pass	

	
