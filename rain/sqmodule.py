import sqlite3
import datetime

def create_table(sqlite_file):
	conn = sqlite3.connect(sqlite_file, detect_types=sqlite3.PARSE_DECLTYPES)
	print "Opened database successfully";

	conn.execute('''CREATE TABLE log
		   (DATE		   TEXT		NOT NULL,
			TIME		   TEXT 	NOT NULL,
			RAIN           INT   	NOT NULL,
		    TEMP           REAL     NOT NULL,
		    HUM       	   REAL		NOT NULL,
		    DUST       	   REAL		NOT NULL);''')
	print "Table created successfully";

	conn.close()


def create_table_UV(sqlite_file):
	conn = sqlite3.connect(sqlite_file, detect_types=sqlite3.PARSE_DECLTYPES)
	print "Opened database successfully";

	conn.execute('''CREATE TABLE logUV
		   (DATE		   TEXT		NOT NULL,
			TIME		   TEXT 	NOT NULL,
			ADC            INT   	NOT NULL,
		    VOLT           REAL     NOT NULL,
		    UVI       	   REAL		NOT NULL);''')

	print "Table created successfully";

	conn.close()

def create_table_UVsg(sqlite_file):
	conn = sqlite3.connect(sqlite_file, detect_types=sqlite3.PARSE_DECLTYPES)
	print "Opened database successfully";

	conn.execute('''CREATE TABLE loguvsg
		   (DATE		   TEXT		NOT NULL,
			TIME		   TEXT 	NOT NULL,
			ADC            INT   	NOT NULL,
		    VOLT           REAL     NOT NULL,
		    UVI       	   REAL		NOT NULL);''')

	print "Table created successfully";

	conn.close()

def create_table_UVinpe(sqlite_file):
	conn = sqlite3.connect(sqlite_file, detect_types=sqlite3.PARSE_DECLTYPES)
	print "Opened database successfully";

	conn.execute('''CREATE TABLE loginpe
		   (DATE		   TEXT		NOT NULL,
			TIME		   TEXT 	NOT NULL,
		    UVI       	   INT		NOT NULL);''')

	print "Table created successfully";

	conn.close()
	
def insert_row(sqlite_file,table_name,data):

	conn = sqlite3.connect('files/logs/%s' % sqlite_file)
	print "Opened database successfully";


	query = """
			INSERT INTO log (DATE,TIME,RAIN,TEMP,HUM,DUST) \
			VALUES (?,?,?,?,?,?)
			"""
			
	now = datetime.datetime.now()
	timeString = now.strftime("%Y-%m-%d %H:%M:%S")

	print timeString.split(" ")

	data = [timeString.split(" ")[0],timeString.split(" ")[1],data['water_level'],data['temperature'],data['humidity'],data['dust_density']]
	
	conn.execute(query,data);
		
	conn.commit()
	print "Records created successfully";
	conn.close()
	

def insert_row_UV(sqlite_file,table_name,message):
	conn = sqlite3.connect('files/logs/%s' % sqlite_file)
	print "Opened database successfully";

	query = """
			INSERT INTO loguv (DATE,TIME,ADC,VOLT,UVI) \
			VALUES (?,?,?,?,?)
			"""

	
	#print "SQL DATA TO BE SAVED UV" + message

	now = datetime.datetime.now()
	timeString = now.strftime("%Y-%m-%d %H:%M:%S")
	data = [timeString.split(" ")[0],timeString.split(" ")[1],message.split ("\t")[0],message.split ("\t")[1],message.split ("\t")[2]]
	print data	
	conn.execute(query,data);
	
	
	conn.commit()
	print "Records created successfully";
	conn.close()

def insert_row_UVsg(sqlite_file,table_name,message):

	conn = sqlite3.connect('files/logs/%s' % sqlite_file)
	print "Opened database successfully";

	query = """
			INSERT INTO loguvsg (DATE,TIME,ADC,VOLT,UVI) \
			VALUES (?,?,?,?,?)
			"""
	
	#print "SQL DATA TO BE SAVED UV" + message

	now = datetime.datetime.now()
	timeString = now.strftime("%Y-%m-%d %H:%M:%S")
	data = [timeString.split(" ")[0],timeString.split(" ")[1],message.split ("\t")[6],message.split ("\t")[7],message.split ("\t")[8]]
	
	conn.execute(query,data);
	
	
	conn.commit()
	print "Records created successfully";
	conn.close()

def insert_row_UVinpe(sqlite_file,table_name,message):

	conn = sqlite3.connect('files/logs/%s' % sqlite_file)
	print "Opened database successfully";

	query = """
			INSERT INTO loginpe (DATE,TIME,UVI) \
			VALUES (?,?,?)
			"""
	
	#print "SQL DATA TO BE SAVED UV" + message

	now = datetime.datetime.now()
	timeString = now.strftime("%Y-%m-%d %H:%M:%S")
	data = [timeString.split(" ")[0],message.split ("\t")[0],message.split ("\t")[1]]
	
	conn.execute(query,data);
	
	
	conn.commit()
	print "Records created successfully";
	conn.close()

def connect(sqlite_file):
    """ Make connection to an SQLite database file """
    conn = sqlite3.connect(sqlite_file)
    c = conn.cursor()
    return conn, c

def close(conn):
    """ Commit changes and close connection to the database """
    # conn.commit()
    conn.close()

def total_rows(cursor, table_name, print_out=False):
    """ Returns the total number of rows in the database """
    c.execute('SELECT COUNT(*) FROM {}'.format(table_name))
    count = c.fetchall()
    if print_out:
        print('\nTotal rows: {}'.format(count[0][0]))
    return count[0][0]

def table_col_info(cursor, table_name, print_out=False):
    """ Returns a list of tuples with column informations:
        (id, name, type, notnull, default_value, primary_key)
    """
    c.execute('PRAGMA TABLE_INFO({})'.format(table_name))
    info = c.fetchall()

    if print_out:
        print("\nColumn Info:\nID, Name, Type, NotNull, DefaultVal, PrimaryKey")
        for col in info:
            print(col)
    return info
    
# Adding a new column to save date and time and update with current date-time
# in the following format: YYYY-MM-DD HH:MM:SS
# e.g., 2014-03-06 16:26:37
def add_datetimeColumn(sqlite_file, tablename):
	
	conn = sqlite3.connect(sqlite_file)
	c = conn.cursor()
	print "Opened database successfully";

	c.execute("ALTER TABLE {tn} ADD COLUMN '{cn}'"\
			 .format(tn=table_name, cn='date_time'))
	# update row for the new current date and time column, e.g., 2014-03-06 16:26:37
	c.execute("UPDATE {tn} SET {cn}=(CURRENT_TIMESTAMP) WHERE {idf}='some_id1'"\
			 .format(tn=table_name, idf='id', cn='date_time'))

	conn.close()
	

def values_in_col(cursor, table_name, print_out=True):
    """ Returns a dictionary with columns as keys and the number of not-null
        entries as associated values.
    """
    c.execute('PRAGMA TABLE_INFO({})'.format(table_name))
    info = c.fetchall()
    col_dict = dict()
    for col in info:
        col_dict[col[1]] = 0
    for col in col_dict:
        c.execute('SELECT ({0}) FROM {1} WHERE {0} IS NOT NULL'.format(col, table_name))
        # In my case this approach resulted in a better performance than using COUNT
        number_rows = len(c.fetchall())
        col_dict[col] = number_rows
    if print_out:
        print("\nNumber of entries per column:")
        for i in col_dict.items():
            print('{}: {}'.format(i[0], i[1]))
    return col_dict


def query_last(sqlite_file,table_name):
	conn = sqlite3.connect('files/logs/%s' % sqlite_file)
	print "Opened database successfully"
	cursor = conn.execute('SELECT max(ROWID) FROM log')
	max_id = cursor.fetchone()[0]
	#print max_id
	
	cursor = conn.execute("SELECT * FROM log WHERE ROWID=:Id",{"Id": max_id})   
	last_row = cursor.fetchall()[0]
	#print last_row
	
	conn.close()
	return (last_row)

def query_last_rain(sqlite_file,table_name):
	conn = sqlite3.connect('files/logs/%s' % sqlite_file)
	print "Opened database successfully"
	row = "nao ha","registros"
	for row in conn.execute("SELECT DATE,TIME FROM log WHERE RAIN<1010"):
		#print row
		pass
	conn.close()
	return (row)

def query_last_UV(sqlite_file,table_name):
	conn = sqlite3.connect('files/logs/%s' % sqlite_file)
	print "Opened database successfully"
	cursor = conn.execute('SELECT max(ROWID) FROM logUV')
	max_id = cursor.fetchone()[0]
	#print max_id
	
	cursor = conn.execute("SELECT * FROM logUV WHERE ROWID=:Id",{"Id": max_id})   
	last_row = cursor.fetchall()[0]
	#print last_row
	
	conn.close()
	return (last_row)

def query_last_UVsg(sqlite_file,table_name):
	conn = sqlite3.connect('files/logs/%s' % sqlite_file)
	print "Opened database successfully"
	cursor = conn.execute('SELECT max(ROWID) FROM loguvsg')
	max_id = cursor.fetchone()[0]
	#print max_id
	
	cursor = conn.execute("SELECT * FROM loguvsg WHERE ROWID=:Id",{"Id": max_id})   
	last_row = cursor.fetchall()[0]
	#print last_row
	
	conn.close()
	return (last_row)

def query_last_UVinpe(sqlite_file,table_name):
	conn = sqlite3.connect('files/logs/%s' % sqlite_file)
	print "Opened database successfully"
	cursor = conn.execute('SELECT max(ROWID) FROM loginpe')
	max_id = cursor.fetchone()[0]
	#print max_id
	
	cursor = conn.execute("SELECT * FROM loginpe WHERE ROWID=:Id",{"Id": max_id})   
	last_row = cursor.fetchall()[0]
	#print last_row
	
	conn.close()
	return (last_row)


if __name__ == '__main__':
	
	sqlite_file = 'rainlog.sqlite'
	table_name = 'log'
	#table_name = 'logUV'
	table_name = 'loginpe'
	

	# create_table(sqlite_file)
	# create_table_UV(sqlite_file)
	# create_table_UVsg(sqlite_file)
	#create_table_UVinpe(sqlite_file)

	#insert_row_UV(sqlite_file,table_name,"99	99.99	1.111")
	#insert_row_UVsg(sqlite_file,table_name,"99	99.99	9.999	11	11.11	1.111	22	22.22	2.222")
	#insert_row(sqlite_file,table_name,"00:00:00	1024	0	01.0	99.9	999.9")

	#add_datetimeColumn(sqlite_file,table_name)
	
	#print(query_last_UV(sqlite_file,table_name))
	#print(query_last_UVsg(sqlite_file,table_name))
	#print query_last(sqlite_file,table_name)
	#print query_last_UVinpe(sqlite_file,table_name)
    #conn, c = connect(sqlite_file)
    
    
    
    #close(conn)
